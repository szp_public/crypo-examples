# Crypto examples

## Docker

Argon2 cryptografy using the `abstractvector/argon2` docker image. Take a look into the `Makefile`


## Java

A java project using argon2. Take a look into the following example `./argon2-example/src/main/java/szp/rafael/crypto/CryptoExample.java`
