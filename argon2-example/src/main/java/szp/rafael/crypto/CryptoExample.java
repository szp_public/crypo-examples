package szp.rafael.crypto;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Helper;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class CryptoExample {

  public static final int SALT_LEN=32;
  public static final int HASH_LEN=32;
  public static final int ITERATIONS = 19;
  public static final int MEMORY = 128*1024;
  public static final int PARALLELISM = 4;
  public static final int MAX_MILLISECS = 1000;

  static Argon2 ARGON2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id,SALT_LEN,HASH_LEN);

  public static void main(String... args){


    String password = "MyVerySecurePassword";

    Instant start = Instant.now();

    String hash = encrypt(password.toCharArray());
    boolean isMatched = verifyPassword(hash,password.toCharArray());
    System.out.println("Does password match? "+isMatched);
    Instant end = Instant.now();
    long between = ChronoUnit.MILLIS.between(start, end);
    System.out.println("Took "+between+"ms");
    System.out.printf("Split: %s",splitHash(hash));
//    int iterations = Argon2Helper.findIterations(ARGON2, MAX_MILLISECS, MEMORY, PARALLELISM);
//
//    System.out.println("Optimal number of iterations: " + iterations);

  }

  private static boolean verifyPassword(String hash,char[] password) {
    System.out.println("Verifying "+hash+ " with the password "+new String(password));
    boolean verify = ARGON2.verify(hash, password);
    return verify;
  }

  private static String encrypt(char[] password) {
    String hash="";
    try{
      hash = ARGON2.hash(ITERATIONS, MEMORY, PARALLELISM, password);
      System.out.println(hash);
    }finally {
      ARGON2.wipeArray(password);
      return hash;
    }
  }

  private static String[] splitHash(String hash){
    String[] split = hash.split("\\$");
    return split;
  }



}
